FROM ubuntu
MAINTAINER R0flcpt3r

RUN apt update && \
	apt -y install wget git

RUN mkdir /blog
WORKDIR /blog
RUN wget https://github.com/gohugoio/hugo/releases/download/v0.65.3/hugo_extended_0.65.3_Linux-64bit.tar.gz
RUN tar xzf hugo_extended_0.65.3_Linux-64bit.tar.gz
COPY . ./
RUN git submodule init
RUN git submodule sync
RUN git submodule update
RUN ./hugo

FROM abiosoft/caddy

COPY --from=0 /blog/public /srv/html
COPY caddyfile /etc/Caddyfile

ENTRYPOINT ["/bin/parent", "caddy"]
CMD ["--conf", "/etc/Caddyfile", "--log", "stdout", "--agree=$ACME_AGREE"]
