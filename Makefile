##
# Blog
#
# @file
# @version 0.1

.PHONY: test deploy build

build: ./content/
	hugo

deploy: build ./public/
	scp -r public/* lab_hv%r:/virtpool/docker/blog/public_html

test:
	hugo server -D

# end
